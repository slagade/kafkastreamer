package Kafkaconn


import java.lang.Long
import java.util.Properties
import java.util.concurrent.TimeUnit

import org.apache.kafka.common.serialization._
import org.apache.kafka.streams._
import org.apache.kafka.streams.kstream.{KStream, KStreamBuilder, KTable}
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.connect.json.JsonDeserializer;
import org.apache.kafka.connect.json.JsonSerializer;
import scala.language.implicitConversions
import scala.collection.JavaConverters.asJavaIterableConverter
import org.apache.kafka.streams.processor.AbstractProcessor;
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HColumnDescriptor
import org.apache.hadoop.hbase.HTableDescriptor
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.util.Bytes
import org.apache.kafka.streams.kstream.ForeachAction


object Kconn {
	def main(args: Array[String]) {

	   // Logger
    case class Mess(key: Long, value: String)
    val configuration:Configuration = new Configuration();
		//String kafkaJaasConfPath=args[3];

		System.setProperty("java.security.krb5.conf", "D:\\keytabs\\krb5.conf")
		System.setProperty("java.security.auth.login.config","D:\\keytabs\\kafka_jaas.conf")
		System.setProperty("javax.security.auth.useSubjectCredsOnly", "false")
		System.setProperty("sun.security.krb5.debug", "true")
		println("Reading messsages from topic- topic1"); 
		
		val streamsConfiguration: Properties = {
				val p = new Properties()
						p.put(StreamsConfig.APPLICATION_ID_CONFIG, "group1")
						p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "vvhwdevnode1.aws.virginvoyages.com:6667")
						p.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName())
						p.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName())
						p.put("auto.offset.reset","earliest")
						p.put(StreamsConfig.ZOOKEEPER_CONNECT_CONFIG, "vvhwdevnode1.aws.virginvoyages.com:2181,vvhwdevnode2.aws.virginvoyages.com:2181,vvhwdevnode3.aws.virginvoyages.com:2181")
						p.put("enable.auto.commit", "true")
						p.put("auto.commit.interval.ms", "1000")
						p.put("security.protocol", "SASL_PLAINTEXT")
						//p.put("group.id", "group1")		
						p
		}
		val stringSerde: Serde[String] = Serdes.String()
				val longSerde: Serde[Long] = Serdes.Long()
				
				val builder: KStreamBuilder = new KStreamBuilder()
		    //import KeyValueImplicits._
				val textLines: KStream[String, String] = builder.stream("topic1")
				//.map((key , value) -> new KeyValue<>(key,value)).through("tempartition")
				textLines.print()
				  //some print
			//	textLines
		  
		
       /* textLines.toStream().process(() -> new AbstractProcessor<String, Long>() {
            @Override
            public void process(String user, Long numEdits) {
                System.out.println("USER: " + user + " num.edits: " + numEdits);
            }
        });*/
				
			  /*val messJsonDeserializer: JsonDeserializer<Mess>  new JsonDeserializer<>(Mess.class);
        JsonSerializer<Purchase> purchaseJsonSerializer = new JsonSerializer<>();
*/
				
				//textLines.transformValues(k:String, v:String)
				
				
			/*	
				val str:String = textLines.toString()
				println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+str)*/
				
				//val table:HTable = new HTable(configuration, "consolemsg");
        //val put:Put = new Put(Bytes.toBytes(1));
       // put.add(textLines)
      //  put.add(Bytes.toBytes(colFamName), Bytes.toBytes(colQualifier), Bytes.toBytes(textLines));
       //   put.add(Bytes.toBytes("c1"), Bytes.toBytes("1"), Bytes.toBytes(textLines));
       // table.put(put);
       // table.flushCommits();
        //table.close();

				val streams: KafkaStreams = new KafkaStreams(builder, streamsConfiguration)
						streams.start()

						print("############## we made it till here ################")
						/*
    Runtime.getRuntime.addShutdownHook(new Thread(() => {
      streams.close(10, TimeUnit.SECONDS)
    }))*/
	}
	
	
	
}