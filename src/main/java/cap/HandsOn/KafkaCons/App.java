package cap.HandsOn.KafkaCons;


import java.lang.*;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class App 
{
	public static void main( String[] args)
	{
		
		try{
			System.setProperty("java.security.krb5.conf", "D:\\keytabs\\krb5.conf");
			System.setProperty("java.security.auth.login.config","D:\\keytabs\\kafka_jaas.conf");
			//D:\\keytabs\\krb5.conf
			//System.setProperty("java.security.krb5.conf", args[0]);
			//D:\\keytabs\\kafka_jaas.conf
			//System.setProperty("java.security.auth.login.config",args[1]);
			System.setProperty("javax.security.auth.useSubjectCredsOnly", "false");
			System.setProperty("sun.security.krb5.debug", "true");
			System.out.println("Reading messsages from topic- topic1"); 

			//System.out.println(args[0]);
			//System.out.println(args[1]);
			Properties p = new Properties();
			//p.put(StreamsConfig.APPLICATION_ID_CONFIG, "weblog-consumer");
			p.put("bootstrap.servers", "vvhwdevnode1.aws.virginvoyages.com:6667");
			p.put("auto.offset.reset","earliest");
			p.put("ZOOKEEPER_CONNECT_CONFIG", "vvhwdevnode1.aws.virginvoyages.com:2181,vvhwdevnode2.aws.virginvoyages.com:2181,vvhwdevnode3.aws.virginvoyages.com:2181");
			p.put("enable.auto.commit", "true");
			p.put("auto.commit.interval.ms", "1000");
			p.put("security.protocol", "SASL_PLAINTEXT");
			p.put("session.timeout.ms", "30000");
			p.put("key.deserializer",
					"org.apache.kafka.common.serialization.StringDeserializer");
			p.put("value.deserializer",
					"org.apache.kafka.common.serialization.StringDeserializer");
			
			//Logger.getLogger("org").setLevel(Level.OFF);
	        //Logger.getLogger("akka").setLevel(Level.OFF);
			
			p.put("group.id", "group1");
		
			KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<String, String>(p);
			kafkaConsumer.subscribe(Arrays.asList("topic1"));
			System.out.println("We are before while");
			while (true) {
				ConsumerRecords<String, String> records = kafkaConsumer
						.poll(Integer.parseInt("1"));
				
				for (ConsumerRecord<String, String> record : records) {
					System.out.println("We are in loop");
					System.out.println("offset = %d, value = %s" +
							record.offset() + record.value());
					System.out.println();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
