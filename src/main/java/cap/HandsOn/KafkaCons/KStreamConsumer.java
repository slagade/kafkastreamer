package cap.HandsOn.KafkaCons;


import java.util.Date;
import java.util.Properties;

import cap.HandsOn.KafkaCons.*;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.KTable;
import org.joda.time.DateTime;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.apache.kafka.connect.json.JsonSerializer;


public class KStreamConsumer {
	static public class CountryMessage {
        /* the JSON messages produced to the countries Topic have this structure:
         { "name" : "The Netherlands"
         , "code" : "NL
         , "continent" : "Europe"
         , "population" : 17281811
         , "size" : 42001
         };
   
        this class needs to have at least the corresponding fields to deserialize the JSON messages into
        */
 
        public String code;
        public String name;
        public int population;
        public int size;
        public String continent;
    }

	public static void main( String[] args)
	{
		
		try{
			System.setProperty("java.security.krb5.conf", "D:\\keytabs\\krb5.conf");
			System.setProperty("java.security.auth.login.config","D:\\keytabs\\kafka_jaas.conf");
			//System.setProperty("java.security.auth.login.config","D:\\keytabs\\hbase_jaas.conf");
			//D:\\keytabs\\krb5.conf
			//System.setProperty("java.security.krb5.conf", args[0]);
			//D:\\keytabs\\kafka_jaas.conf
			//System.setProperty("java.security.auth.login.config",args[1]);
			System.setProperty("javax.security.auth.useSubjectCredsOnly", "false");
			System.setProperty("sun.security.krb5.debug", "true");
			System.out.println("Reading messsages from topic- kafkatest"); 

			//System.out.println(args[0]);
			//System.out.println(args[1]);
			Properties p = new Properties();
			p.put(StreamsConfig.APPLICATION_ID_CONFIG, "group1");
			//p.put(StreamsConfig.APPLICATION_ID_CONFIG, "connect-local-file-sink");
			p.put("bootstrap.servers", "vvhwdevnode1.aws.virginvoyages.com:6667");
			p.put("auto.offset.reset","earliest");
			p.put("ZOOKEEPER_CONNECT_CONFIG", "vvhwdevnode1.aws.virginvoyages.com:2181,vvhwdevnode2.aws.virginvoyages.com:2181,vvhwdevnode3.aws.virginvoyages.com:2181");
			p.put("enable.auto.commit", "true");
			p.put("auto.commit.interval.ms", "1000");
			p.put("security.protocol", "SASL_PLAINTEXT");
			p.put("session.timeout.ms", "30000");
			p.put("key.deserializer",
					"org.apache.kafka.common.serialization.StringDeserializer");
			p.put("value.deserializer",
					"org.apache.kafka.common.serialization.StringDeserializer");
			
			//Logger.getLogger("org").setLevel(Level.OFF);
	        //Logger.getLogger("akka").setLevel(Level.OFF);
			// Set up serializers and deserializers, which we will use for overriding the default serdes
		    // specified above.
		    final Serde<String> stringSerde = Serdes.String();
		    final Serde<byte[]> byteArraySerde = Serdes.ByteArray();

		    // In the subsequent lines we define the processing topology of the Streams application.
		    final KStreamBuilder builder = new KStreamBuilder();

		    // Read the input Kafka topic into a KStream instance.
		    final KStream<byte[], String> textLines = builder.stream(byteArraySerde, stringSerde, "kafkatest");
		    textLines.print();
		    //textLines.map((key, value) -> new KeyValue<>(key, HbaseClient.addRecord("consolemsg", "r2", "msg", "data", value) ));
		    
		    long rowL = new DateTime().getMillis();
		    String row = Long.toString(rowL);
		    textLines.mapValues(value ->  {
				try {
					return HbaseClient.addRecord("consolemsg", row, "msg", "data", value);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return value;
			} );

		    
		    final KafkaStreams streams = new KafkaStreams(builder, p);
		    streams.cleanUp();
		    streams.start();
					
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	


}
