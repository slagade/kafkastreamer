package cap.HandsOn.KafkaCons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation; 


public class HbaseClient {
	private static Configuration conf = null;
	private static Connection connection = null;
	private static Table table = null;
	private static TableName TABLE_NAME = TableName.valueOf("consolemsg");

	/**
	 * Initialization
	 */
	static {
		conf = HBaseConfiguration.create();
		conf.set("hadoop.security.authentication", "Kerberos");
		conf.set("hbase.zookeeper.quorum", "vvhwdevnode1.aws.virginvoyages.com,vvhwdevnode2.aws.virginvoyages.com,vvhwdevnode3.aws.virginvoyages.com");
		conf.set("hbase.zookeeper.property.clientPort", "2181");
		conf.set("zookeeper.znode.parent", "/hbase-secure");
		conf.set("hbase.cluster.distributed", "true");
		UserGroupInformation.setConfiguration(conf);
		try {
			UserGroupInformation.loginUserFromKeytab("hbase/vvhwdevnode1.aws.virginvoyages.com@VVHWAD.AWS.VIRGINVOYAGES.COM", "D:\\keytabs\\hbase.service.keytab" );
			connection = ConnectionFactory.createConnection(conf);
			//UserGroupInformation.loginUserFromKeytab("hbase@VVHWAD.AWS.VIRGINVOYAGES.COM", "D:\\keytabs\\hbase.service.keytab" );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * Put (or insert) a row
	 */
	public static String addRecord(String tableName, String rowKey,
			String family, String qualifier, String value) throws Exception {
		try {
			table = connection.getTable(TABLE_NAME);
			Put put = new Put(Bytes.toBytes(rowKey));
			put.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier), Bytes
					.toBytes(value));
			table.put(put);
			System.out.println("insert recored " + rowKey + " to table "
					+ tableName + " ok.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "TRUE";
	}

	public static void main(String[] agrs) {
		try {

			
			HbaseClient.addRecord("consolemsg", "r2", "msg", "data", "What next?");
			/*table = connection.getTable(TABLE_NAME);
			Put p = new Put(Bytes.toBytes("r2"));
			p.addColumn(Bytes.toBytes("msg"), Bytes.toBytes("data"), Bytes.toBytes("Nailed!"));
			// send the data.
			table.put(p);
			 */	
			System.out.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}